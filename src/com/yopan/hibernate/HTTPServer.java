package com.yopan.hibernate;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URLDecoder;
import java.util.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import com.sun.net.httpserver.HttpExchange;
import com.yopan.hibernate.models.Game;
import com.yopan.hibernate.models.Player;
import com.yopan.hibernate.models.Points;

public class HTTPServer {

    public static void main(String[] args) throws Exception {

        int portNumber = 8080;

        try {
            HttpServer httpServer = HttpServer.create(new InetSocketAddress(portNumber), 0);
            httpServer.createContext("/", new RootHandler());
            httpServer.createContext("/listPlayers", new ListPlayers());
            httpServer.createContext("/listGames", new ListGames());
            httpServer.createContext("/listPoints", new ListPoints());
            httpServer.createContext("/echoHeader", new EchoHeaderHandler());
            httpServer.createContext("/echoGet", new EchoGetHandler());
            httpServer.createContext("/echoPost", new EchoPostHandler());
            httpServer.setExecutor(null);
            httpServer.start();
        } catch (IOException exception) {
            System.err.println(exception.toString());
        }

        System.out.println("Server is running on port: " + portNumber);
    }

    static class ListPlayers implements HttpHandler {
        @Override
        public void handle(HttpExchange httpExchange) throws IOException {

            Query query = Query.getInstance();
            List<Player> players = query.getPlayers();

            String playersAsJson = "";

            try {
                playersAsJson = playersToJson(players);
            } catch (JsonProcessingException exception) {
                System.err.println(exception.toString());
            }

            httpExchange.sendResponseHeaders(200, playersAsJson.getBytes().length);
            OutputStream outputStream = httpExchange.getResponseBody();
            outputStream.write(playersAsJson.getBytes());
            outputStream.close();
        }
    }

    static class ListGames implements HttpHandler {
        @Override
        public void handle(HttpExchange httpExchange) throws IOException {

            Query query = Query.getInstance();
            List<Game> games = query.getGames();

            String gamesAsJson = "";

            try {
                gamesAsJson = gamesToJson(games);
            } catch (JsonProcessingException exception) {
                System.err.println(exception.toString());
            }

            httpExchange.sendResponseHeaders(200, gamesAsJson.getBytes().length);
            OutputStream outputStream = httpExchange.getResponseBody();
            outputStream.write(gamesAsJson.getBytes());
            outputStream.close();
        }
    }

    static class ListPoints implements HttpHandler {
        @Override
        public void handle(HttpExchange httpExchange) throws IOException {

            Query query = Query.getInstance();
            List<Points> points = query.getPoints();

            String pointsAsJson = "";

            try {
                pointsAsJson = pointsToJson(points);
            } catch (JsonProcessingException exception) {
                System.err.println(exception.toString());
            }

            httpExchange.sendResponseHeaders(200, pointsAsJson.getBytes().length);
            OutputStream outputStream = httpExchange.getResponseBody();
            outputStream.write(pointsAsJson.getBytes());
            outputStream.close();
        }
    }

    static class RootHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            String response = "<h1>Server has started successfully</h1>" + "<h2>Port: 8080</h2>";
            httpExchange.sendResponseHeaders(200, response.length());
            OutputStream os = httpExchange.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

    static class EchoHeaderHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            Headers headers = httpExchange.getRequestHeaders();
            Set<Map.Entry<String, List<String>>> entries = headers.entrySet();
            String response = "";
            for (Map.Entry<String, List<String>> entry : entries)
                response += entry.toString() + "\n";
            httpExchange.sendResponseHeaders(200, response.length());
            OutputStream os = httpExchange.getResponseBody();
            os.write(response.toString().getBytes());
            os.close();
        }
    }

    static class EchoPostHandler implements HttpHandler {
        @Override

        public void handle(HttpExchange httpExchange) throws IOException {
            // parse request
            Map<String, Object> parameters = new HashMap<String, Object>();
            InputStreamReader isr = new InputStreamReader(httpExchange.getRequestBody(), "utf-8");
            BufferedReader br = new BufferedReader(isr);
            String query = br.readLine();
            parseQuery(query, parameters);

            boolean successful = handleSaveToDatabase(parameters);

            // send response
            String response = "";

            if(successful) {
                response = "Data registered successfully.";
            } else {
                response = "There was an error, please try again.";
            }

            httpExchange.sendResponseHeaders(200, response.length());
            OutputStream os = httpExchange.getResponseBody();
            os.write(response.toString().getBytes());
            os.close();
        }
    }

    public static boolean handleSaveToDatabase(Map<String, Object> parameters) {
        boolean entrySuccessful = false;

        for (Map.Entry param : parameters.entrySet()) {
            String firstKey = (String) param.getKey();
            String firstValue = (String) param.getValue();

            if (firstKey.equals("name")) {
                entrySuccessful = savePlayer(firstValue);
            } else if (firstKey.equals("played_at")) {
                entrySuccessful = saveGame(parameters);
            } else if (firstKey.equals("player_id")) {
                entrySuccessful = savePoints(parameters);
            }
        }

        return entrySuccessful;
    }

    public static boolean savePlayer(String playerName) {
        Query query = Query.getInstance();

        Player newPlayer = buildPlayer(playerName);

        Player newPlayerCreated = query.savePlayer(newPlayer.getName());

        return newPlayerCreated.getName().equals(newPlayer.getName());
    }


    public static boolean saveGame(Map<String, Object> parameters) {
        Query query = Query.getInstance();
        List<Game> games = query.getGames();

        String played_at = "";
        int round = 1;

        if(games.size() > 0) {
            Game maxById = games
                    .stream()
                    .max(Comparator.comparing(Game::getId))
                    .orElseThrow(NoSuchElementException::new);

            int currentId = maxById.getId();

            if (currentId > 0) {
                round = currentId + 1;
            }
        }

        for (Map.Entry param : parameters.entrySet()) {
            String key = (String) param.getKey();
            String value = (String) param.getValue();

            if (key.equals("played_at")) {
                played_at = value;
            }
        }

        Game newGame = query.saveGame(played_at, round);

        return newGame.getPlayedAt().equals(played_at);
    }

    public static boolean savePoints(Map<String, Object> parameters) {
        int player_id = 1;
        int game_id = 1;
        int score = 66;

        for (Map.Entry param : parameters.entrySet()) {
            String key = (String) param.getKey();
            String value = (String) param.getValue();

            if (key.equals("player_id")) {
                player_id = Integer.parseInt(value);
            } else if (key.equals("game_id")) {
                game_id = Integer.parseInt(value);
            } else if (key.equals("score")) {
                score = Integer.parseInt(value);
            }
        }

        Query query = Query.getInstance();
        Points newPoint = query.savePoint(player_id, game_id, score);

        return newPoint.getPlayerId() == player_id;
    }

    static class EchoGetHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            // parse request
            Map<String, Object> parameters = new HashMap<>();
            URI requestedUri = httpExchange.getRequestURI();
            String query = requestedUri.getRawQuery();
            parseQuery(query, parameters);

            // send response
            String response = "";
            for (String key : parameters.keySet())
                response += key + " = " + parameters.get(key) + "\n";
            httpExchange.sendResponseHeaders(200, response.length());
            OutputStream os = httpExchange.getResponseBody();
            os.write(response.toString().getBytes());

            os.close();
        }
    }

    public static void parseQuery(String query, Map<String,
            Object> parameters) throws UnsupportedEncodingException {

        if (query != null) {
            String pairs[] = query.split("[&]");
            for (String pair : pairs) {
                String param[] = pair.split("[=]");
                String key = null;
                String value = null;
                if (param.length > 0) {
                    key = URLDecoder.decode(param[0],
                            System.getProperty("file.encoding"));
                }

                if (param.length > 1) {
                    value = URLDecoder.decode(param[1],
                            System.getProperty("file.encoding"));
                }

                if (parameters.containsKey(key)) {
                    Object obj = parameters.get(key);
                    if (obj instanceof List<?>) {
                        List<String> values = (List<String>) obj;
                        values.add(value);

                    } else if (obj instanceof String) {
                        List<String> values = new ArrayList<>();
                        values.add((String) obj);
                        values.add(value);
                        parameters.put(key, values);
                    }
                } else {
                    parameters.put(key, value);
                }
            }
        }
    }

    public static String playersToJson(List<Player> players) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(players);

        return json;
    }

    public static String gamesToJson(List<Game> games) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(games);

        return json;
    }

    public static String pointsToJson(List<Points> points) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(points);

        return json;
    }

    public static Player buildPlayer(String name) {
        Player player = new Player();
        player.setName(name);

        return player;
    }

}