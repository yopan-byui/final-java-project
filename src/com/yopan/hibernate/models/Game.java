
package com.yopan.hibernate.models;

import javax.persistence.*;

@Entity
@Table(name = "game", schema = "games_ranking")
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "played_at")
    private String played_at;

    @Column(name = "round")
    private int round;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlayedAt() {
        return played_at;
    }

    public void setPlayedAt(String played_at) {
        this.played_at = played_at;
    }


    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }

    public String toString() {
        return "Id: " + Integer.toString(id) + " - When: " + played_at + " - Round: " + round;
    }
}