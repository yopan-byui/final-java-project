package com.yopan.hibernate.models;

import javax.persistence.*;

@Entity
@Table(name = "points", schema = "games_ranking")
public class Points {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "player_id")
    private int player_id;

    @Column(name = "game_id")
    private int game_id;

    @Column(name = "score")
    private int score;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPlayerId() {
        return player_id;
    }

    public void setPlayerId(int player_id) {
        this.player_id = player_id;
    }

    public int getGameId() {
        return game_id;
    }

    public void setGameId(int game_id) {
        this.game_id = game_id;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String toString() {
        return "Id: " + Integer.toString(id) + " - Player Id: " + player_id  + " - Game Id: " + game_id + " - Points gotten: " + score;
    }
}