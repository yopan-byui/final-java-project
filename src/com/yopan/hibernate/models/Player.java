package com.yopan.hibernate.models;

import javax.persistence.*;

@Entity
@Table(name = "player", schema = "games_ranking")
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return "Id: " + Integer.toString(id) + " - Player: " + name;
    }
}