package com.yopan.hibernate;

import java.util.*;

import java.net.*;
import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.yopan.hibernate.models.Game;
import com.yopan.hibernate.models.Player;
import com.yopan.hibernate.models.Points;

public class HTTPClient {

    public static void main(String[] args) {
        application();
    }

    public static void application() {
        Scanner scanner = new Scanner(System.in);

        boolean keepRunning = true;
        System.out.println("Welcome to 6 Nimmt! Ranking");
        System.out.println("###########################");
        System.out.println();

        do {

            System.out.println("########## MENU ############");
            System.out.println("Choose the number of your option below");
            System.out.println("#1 - List registered players");
            System.out.println("#2 - List all matches");
            System.out.println("#3 - List player's points per match/game");
            System.out.println("#4 - Create new player");
            System.out.println("#5 - Add new match/game");
            System.out.println("#6 - Add player's points per match");
            System.out.println("#9 - Leave application (exit)");

            try {
                String optionChose = handleInput(scanner);

                keepRunning = handleOption(optionChose);
            } catch (Exception e) {
                System.out.println("Sorry, an error occurred.");
            } finally {
                if(keepRunning) {
                    System.out.println();
                } else {
                    System.out.println("Success! See you later.");
                }
            }

        } while (keepRunning);
    }

    public static String postHttpContent(Map<String, String> params) throws Exception {
        try {

            URL url = new URL("http://localhost:8080/echoPost");
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

            byte[] postDataBytes = new byte[0];

            try {
                postDataBytes = buildPostDataBytes(params);
            } catch (UnsupportedEncodingException exception) {
                System.err.println(exception.toString());
            }

            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            httpURLConnection.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
            httpURLConnection.setDoOutput(true);
            httpURLConnection.getOutputStream().write(postDataBytes);

            String data = "";

            try(BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(httpURLConnection.getInputStream(), "UTF-8"))
            ) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;

                while ((responseLine = bufferedReader.readLine()) != null) {
                    response.append(responseLine.trim());
                }

                data = response.toString();

            } catch (Exception exception) {
                System.err.println(exception.toString());
            }

            return data;

        } catch (IOException e) {
            System.err.println(e.toString());
        }

        return "Error";
    }

    public static byte[] buildPostDataBytes(Map<String, String> params) throws UnsupportedEncodingException {
        StringBuilder postData = new StringBuilder();

        for (Map.Entry param : params.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode((String) param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");

        return postDataBytes;
    }

    public static String getHttpContent(String string, String json) {
        try {

            URL url = new URL(string);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setDoOutput(true);

            try(OutputStream outputStream = httpURLConnection.getOutputStream()) {
                byte[] input = json.getBytes("UTF-8");
                outputStream.write(input, 0, input.length);
            } catch (Exception exception) {
                System.err.println(exception.toString());
            }

            String data = "";

            try(BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(httpURLConnection.getInputStream(), "UTF-8"))
            ) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;

                while ((responseLine = bufferedReader.readLine()) != null) {
                    response.append(responseLine.trim());
                }

                data = response.toString();

            } catch (Exception exception) {
                System.err.println(exception.toString());
            }

            return data;

        } catch (IOException e) {
            System.err.println(e.toString());
        }

        return "Error";
    }

    public static boolean handleOption(String optionChose) {
        boolean keepRunning = true;

        if (optionChose.equals("9")) {
            keepRunning = false;
        } else if (optionChose.equals("1")) {
            printPlayersList();
        } else if (optionChose.equals("2")) {
            printGamesList();
        } else if (optionChose.equals("3")) {
            printPointsList();
        } else if (optionChose.equals("4")) {
            String response = createPlayer();
            System.out.println(response);
        } else if (optionChose.equals("5")) {
            String response = createGame();
            System.out.println(response);
        } else if (optionChose.equals("6")) {
            String response = createPointsRecord();
            System.out.println(response);
        }

        return keepRunning;
    }

    public static String createPlayer() {
        String newPlayerName = handlePlayerInput();

        Map<String, String> newPlayerParams = buildPlayerParams(newPlayerName);

        String response = "There was an error, please try again.";

        try {
            response = HTTPClient.postHttpContent(newPlayerParams);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    public static String createGame() {
        String newMatchDate = handleDateInput();

        Map<String, String> newMatchParams = buildGameParams(newMatchDate);

        String response = "There was an error, please try again.";

        try {
            response = HTTPClient.postHttpContent(newMatchParams);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    public static String createPointsRecord() {
        int playerId = handlePlayerIdInput();
        int gameId = handleGameIdInput();
        int score = handleScoreInput();

        Map<String, String> newPointsParams = buildPointsParams(playerId, gameId, score);

        String response = "There was an error, please try again.";

        try {
            response = HTTPClient.postHttpContent(newPointsParams);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    public static Map<String, String> buildPlayerParams(String name) {
        Map<String, String> params = new LinkedHashMap();
        params.put("name", name);

        return params;
    }

    public static Map<String, String> buildGameParams(String matchDate) {
        Map<String, String> params = new LinkedHashMap();
        params.put("played_at", matchDate);

        return params;
    }

    public static Map<String, String> buildPointsParams(int player_id, int game_id, int score) {
        Map<String, String> params = new LinkedHashMap();
        params.put("player_id", String.valueOf(player_id));
        params.put("game_id", String.valueOf(game_id));
        params.put("score", String.valueOf(score));

        return params;
    }

    public static String handleInput(Scanner scanner) {
        boolean invalidInput = true;
        String input;

        System.out.print("Please press your option (#): ");
        do {
            input = scanner.nextLine();

            if(isInputANumber(input)) {
                invalidInput = false;
            } else {
                System.out.println("That was not a valid option. Please try again.");
                System.out.print("Please press your option (#): ");
            }

        } while (invalidInput);

        return input;
    }

    public static String handlePlayerInput() {
        Scanner scanner = new Scanner(System.in);

        boolean invalidInput = true;
        String playerName;

        System.out.print("Please provide player's full name: ");
        do {
            playerName = scanner.nextLine();

            if(isNameValid(playerName)) {
                invalidInput = false;
            } else {
                System.out.println("That was not a valid name. Please try again.");
                System.out.print("Please provide player's full name: ");
            }

        } while (invalidInput);

        return playerName;
    }

    public static String handleDateInput() {
        Scanner scanner = new Scanner(System.in);

        boolean invalidInput = true;
        String date;

        System.out.print("Please provide match's date (yyyy-mm-dd): ");
        do {
            date = scanner.nextLine();

            if(isDateValid(date)) {
                invalidInput = false;
            } else {
                System.out.println("That was not a valid date. Please try again.");
                System.out.print("Please provide match's date (yyyy-mm-dd): ");
            }

        } while (invalidInput);

        return date;
    }

    public static int handlePlayerIdInput() {
        Scanner scanner = new Scanner(System.in);

        boolean invalidInput = true;
        String playerId;

        printPlayersList();
        System.out.print("Please provide player's Id (integer): ");
        do {
            playerId = scanner.nextLine();

            if(isInputANumber(playerId)) {
                invalidInput = false;
            } else {
                System.out.println("That was not a valid Player ID. Please try again.");
                System.out.print("Please provide player's Id (integer): ");
            }

        } while (invalidInput);

        return Integer.parseInt(playerId);
    }

    public static int handleGameIdInput() {
        Scanner scanner = new Scanner(System.in);

        boolean invalidInput = true;
        String gameId;

        printGamesList();
        System.out.print("Please provide match/game Id (integer): ");
        do {
            gameId = scanner.nextLine();

            if(isInputANumber(gameId)) {
                invalidInput = false;
            } else {
                System.out.println("That was not a valid match/game ID. Please try again.");
                System.out.print("Please provide match/game Id (integer): ");
            }

        } while (invalidInput);

        return Integer.parseInt(gameId);
    }

    public static int handleScoreInput() {
        Scanner scanner = new Scanner(System.in);

        boolean invalidInput = true;
        String score;

        System.out.print("Please insert new player's score (integer): ");
        do {
            score = scanner.nextLine();

            if(isInputANumber(score)) {
                invalidInput = false;
            } else {
                System.out.println("That was not a valid score. Please try again.");
                System.out.print("Please insert new player's score (integer): ");
            }

        } while (invalidInput);

        return Integer.parseInt(score);
    }

    public static boolean isInputANumber (String input) {
        try {
            Float.parseFloat(input);
        } catch(NumberFormatException e) {
            return false;
        }
        return true;
    }

    public static boolean isNameValid(String string) {
        String usernameRegex = "^[\\p{L} .'-]+$";

        Pattern pattern = Pattern.compile(usernameRegex);

        if (string == null) {
            return false;
        }

        Matcher matcher = pattern.matcher(string);
        return matcher.matches();
    }


    public static boolean isDateValid(String string) {
        String dateRegex = "^\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12][0-9]|3[01])$";

        Pattern pattern = Pattern.compile(dateRegex);

        if (string == null) {
            return false;
        }

        Matcher matcher = pattern.matcher(string);
        return matcher.matches();
    }

    public static void printPlayersList () {
        String url = "http://localhost:8080/listPlayers";
        String json = "[{\"key\":\"value\", \"key\":\"value\"}]";

        System.out.println();
        System.out.println("List of all registered players:");

        try {
            String response = HTTPClient.getHttpContent(url, json);
            List<Player> players = jsonToPlayers(response);

            System.out.println();
            for (Player player : players) {
                System.out.println(player);
            }
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }

    public static void printGamesList () {
        String url = "http://localhost:8080/listGames";
        String json = "[{\"key\":\"value\", \"key\":\"value\", \"key\":\"value\"}]";

        System.out.println();
        System.out.println("List of all matches/games:");

        try {
            String response = HTTPClient.getHttpContent(url, json);
            List<Game> games = jsonToGames(response);

            System.out.println();
            for (Game game : games) {
                System.out.println(game);
            }
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }

    public static void printPointsList () {
        String url = "http://localhost:8080/listPoints";
        String json = "[{\"key\":\"value\", \"key\":\"value\", \"key\":\"value\"}, \"key\":\"value\"}]";

        System.out.println();
        System.out.println("List player's score by match:");

        try {
            String response = HTTPClient.getHttpContent(url, json);
            List<Points> points = jsonToPoints(response);

            System.out.println();
            for (Points point : points) {
                System.out.println(point);
            }
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }

    public static List<Player> jsonToPlayers(String json) {
        ObjectMapper objectMapper = new ObjectMapper();
        Player[] players = null;

        try {
            players = objectMapper.readValue(json, Player[].class);
        } catch (JsonProcessingException exception) {
            System.err.println(exception.toString());
        }

        return Arrays.asList(players);
    }

    public static List<Game> jsonToGames(String json) {
        ObjectMapper objectMapper = new ObjectMapper();
        Game[] games = null;

        try {
            games = objectMapper.readValue(json, Game[].class);
        } catch (JsonProcessingException exception) {
            System.err.println(exception.toString());
        }

        return Arrays.asList(games);
    }

    public static List<Points> jsonToPoints(String json) {
        ObjectMapper objectMapper = new ObjectMapper();
        Points[] points = null;

        try {
            points = objectMapper.readValue(json, Points[].class);
        } catch (JsonProcessingException exception) {
            System.err.println(exception.toString());
        }

        return Arrays.asList(points);
    }
}
