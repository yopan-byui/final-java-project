package com.yopan.hibernate;

import com.yopan.hibernate.models.Game;
import com.yopan.hibernate.models.Player;
import com.yopan.hibernate.models.Points;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.*;

public class Query {

    SessionFactory factory = null;
    Session session = null;

    private static Query single_instance = null;

    private Query()
    {
        factory = HibernateUtils.getSessionFactory();
    }

    public static Query getInstance()
    {
        if (single_instance == null) {
            single_instance = new Query();
        }

        return single_instance;
    }
    public List<Player> getPlayers() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.yopan.hibernate.models.Player";
            List<Player> players = (List<Player>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return players;

        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    public Player getPlayer(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.yopan.hibernate.models.Player where id=" + Integer.toString(id);
            Player player = (Player)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return player;

        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    public Player savePlayer(String name) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();

            Player player = new Player();
            player.setName(name);

            session.save(player);

            session.getTransaction().commit();

            return player;

        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    public List<Game> getGames() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.yopan.hibernate.models.Game";
            List<Game> games = (List<Game>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return games;

        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    public Game getGame(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.yopan.hibernate.models.Game where id=" + Integer.toString(id);
            Game game = (Game)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return game;

        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    public Game saveGame(String played_at, int round) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();

            Game game = new Game();
            game.setPlayedAt(played_at);
            game.setRound(round);

            session.save(game);

            session.getTransaction().commit();

            return game;

        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    public List<Points> getPoints() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.yopan.hibernate.models.Points";
            List<Points> points = (List<Points>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return points;

        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    public Points getPoint(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.yopan.hibernate.models.Points where id=" + Integer.toString(id);
            Points point = (Points)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return point;

        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    public Points savePoint(int player_id, int game_id, int score) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();

            Points point = new Points();
            point.setPlayerId(player_id);
            point.setGameId(game_id);
            point.setScore(score);

            session.save(point);

            session.getTransaction().commit();

            return point;

        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }
}
