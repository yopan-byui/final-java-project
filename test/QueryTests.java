import com.yopan.hibernate.models.Game;
import com.yopan.hibernate.models.Player;
import com.yopan.hibernate.models.Points;
import com.yopan.hibernate.Query;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Test;

import java.util.List;

@DisplayName("Testing all queries")
public class QueryTests {
    Query query = Query.getInstance();

    @Nested
    @DisplayName("When querying players")
    class PlayerTests {
        String playerName = "John Doe";
        int playerId = 1;

        @Test
        @DisplayName("Should list players")
        public void listPlayers() {
            List<Player> players = query.getPlayers();

            Assertions.assertFalse(players.isEmpty());
        }

        @Test
        @DisplayName("Should fetch player with right ID")
        public void playerById() {
            Player player = query.getPlayer(playerId);

            Assertions.assertEquals(player.getId(), playerId);
        }

        @Test
        @DisplayName("Should return a player when saving new player")
        public void saveNewPlayer() {
            Player player = query.savePlayer(playerName);

            Assertions.assertTrue(player.getName().equals(playerName));
        }
    }

    @Nested
    @DisplayName("When querying points")
    class PointsTests {
        int gameId = 1;
        int playerId = 1;
        int pointsId = 1;
        int score = 66;

        @Test
        @DisplayName("Should list points")
        public void listPoints() {
            List<Points> points = query.getPoints();

            Assertions.assertFalse(points.isEmpty());
        }

        @Test
        @DisplayName("Should fetch point with right ID")
        public void pointById() {
            Points point = query.getPoint(pointsId);

            Assertions.assertEquals(point.getId(), pointsId);
        }

        @Test
        @DisplayName("Should return a point when saving new point")
        public void saveNewPoint() {
            Points point = query.savePoint(playerId, gameId, score);

            Assertions.assertEquals(point.getGameId(), gameId);
            Assertions.assertEquals(point.getPlayerId(), playerId);
            Assertions.assertEquals(point.getScore(), score);
        }
    }

    @Nested
    @DisplayName("When querying games")
    class GamesTests {
        int gameId = 1;
        String played_at = "2021-08-08";
        int round = 0;

        @Test
        @DisplayName("Should list games")
        public void listGames() {
            List<Game> game = query.getGames();

            Assertions.assertFalse(game.isEmpty());
        }

        @Test
        @DisplayName("Should fetch game with right ID")
        public void gameById() {
            Game game = query.getGame(gameId);

            Assertions.assertEquals(game.getId(), gameId);
        }

        @Test
        @DisplayName("Should return a game when saving new game")
        public void saveNewGame() {
            Game game = query.saveGame(played_at, round);

            Assertions.assertEquals(game.getPlayedAt(), played_at);
            Assertions.assertEquals(game.getRound(), round);
        }
    }
}
